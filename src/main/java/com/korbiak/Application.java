package com.korbiak;

import com.korbiak.model.BinaryTreeMap;
import com.korbiak.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
