package com.korbiak.model;

public interface Model {
    int size();
    boolean isEmpty();
    String get(Object key);
    String put(String key, String value);
    String remove(Object key);
    String print();
}
