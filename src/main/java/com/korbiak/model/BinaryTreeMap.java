package com.korbiak.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class BinaryTreeMap<K extends Comparable, V> implements Map<K, V> {

    private int size = 0;

    private Node<K, V> root;

    private static class Node<K, V> implements Map.Entry<K, V> {
        private K key;
        private V value;
        private Node<K, V> left;
        private Node<K, V> right;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }


        @Override
        public K getKey() {
            return this.key;
        }

        @Override
        public V getValue() {
            return this.value;
        }

        @Override
        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return false;
        }
        return true;
    }

    @Override
    public V get(Object key) {
        Comparable<? super K> k = (Comparable<? super K>) key;
        Node<K, V> node = root;
        while (node != null) {
            int cmp = k.compareTo(node.key);
            if (cmp < 0) {
                node = node.left;
            } else if (cmp > 0) {
                node = node.right;
            } else return node.value;
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        Node<K, V> parent = root;
        Node<K, V> tempParent = root;
        if (root == null) {
            root = new Node<>(key, value);
        } else {
            int cmp;
            do {
                parent = tempParent;
                cmp = key.compareTo(tempParent.key);
                if (cmp < 0) {
                    tempParent = tempParent.left;
                } else if (cmp > 0) {
                    tempParent = tempParent.right;
                } else {
                    V oldValue = tempParent.value;
                    tempParent.value = value;
                    return oldValue;
                }
            } while (tempParent != null);
            if (cmp > 0) {
                parent.right = new Node<>(key, value);
            }
            if (cmp < 0) {
                parent.left = new Node<>(key, value);
            }
        }
        size += 1;
        return null;
    }

    @Override
    public V remove(Object key) {
        K k = (K) key;
        Node<K, V> removingNode = root;
        Node<K, V> parent = root;
        Node<K, V> parentForSon = null;
        Node<K, V> sonNode = null;
        if (root == null) {
            return null;
        } else {
            int cmp;
            do {
                cmp = k.compareTo(removingNode.key);
                if (cmp < 0) {
                    if (removingNode.left == null) return null;
                    parent = removingNode;
                    removingNode = removingNode.left;
                } else if (cmp > 0) {
                    if (removingNode.right == null) return null;
                    parent = removingNode;
                    removingNode = removingNode.right;
                } else break;
            } while (true);

            if (removingNode.right == null && removingNode.left == null) {
                if (root == removingNode) root = null;
                else if (parent.left == removingNode) parent.left = null;
                else if (parent.right == removingNode) parent.right = null;
            } else if (removingNode.right == null) {
                if (parent.left == removingNode) parent.left = removingNode.left;
                else parent.right = removingNode.left;
            } else if (removingNode.left == null) {
                if (parent.left == removingNode) parent.left = removingNode.right;
                else parent.right = removingNode.right;
            } else {
                sonNode = removingNode.right;
                while (sonNode.left != null) {
                    parentForSon = sonNode;
                    sonNode = sonNode.left;
                }
                if (parentForSon != null) {
                    parentForSon.left = sonNode.right;
                } else {
                    removingNode.right = null;
                }
                sonNode.left = removingNode.left;
                sonNode.right = removingNode.right;

                if (removingNode != root) {
                    if (parent.left == removingNode) parent.left = sonNode;
                    else parent.right = sonNode;
                } else {
                    root = sonNode;
                }
            }
            size--;
            return removingNode.getValue();
        }
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> set = new HashSet<>();
        runTree(root, set);
        return set;
    }

    private void runTree(Node<K, V> localRoot, Set<Entry<K, V>> set) {
        if (localRoot != null) {
            runTree(localRoot.left, set);
            set.add(localRoot);
            runTree(localRoot.right, set);
        }
    }

    //Don't work
    //----------------------------------------------------------------------------------
    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public void clear() {

    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {

    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }
}
