package com.korbiak.model;

public class BusinessLogic implements Model {
    BinaryTreeMap<String, String> dictionary;

    public BusinessLogic() {
        this.dictionary = new BinaryTreeMap<>();
    }


    @Override
    public int size() {
        return dictionary.size();
    }

    @Override
    public boolean isEmpty() {
        return dictionary.isEmpty();
    }

    @Override
    public String get(Object key) {
        return dictionary.get(key);
    }

    @Override
    public String put(String key, String value) {
        return dictionary.put(key, value);
    }

    @Override
    public String remove(Object key) {
        return dictionary.remove(key);
    }

    @Override
    public String print() {
        String answer = "";
        for (BinaryTreeMap.Entry<String, String> entry : dictionary.entrySet()) {
            answer += ("Word: " + entry.getKey() + ". Meaning: " + entry.getValue());
        }
        return answer;
    }
}
