package com.korbiak.view;

import com.korbiak.Application;
import com.korbiak.controller.Controller;
import com.korbiak.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView  {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    Logger logger;

    public MyView() {
        logger = LogManager.getLogger(Application.class);
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - put new word");
        menu.put("2", "  2 - get word");
        menu.put("3", "  3 - remove word");
        menu.put("4", "  4 - print all words");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        logger.trace("Set word:");
        String word = input.next();
        logger.trace("Set meaning of this word:");
        String meaning = input.next();
        controller.put(word, meaning);
    }

    private void pressButton2() {
        logger.trace("Set word:");
        String word = input.next();
        logger.trace(controller.get(word));
    }

    private void pressButton3() {
        logger.trace("What word do you wont to delete:");
        String word = input.next();
        controller.remove(word);
    }

    private void pressButton4() {
        logger.trace("Your dictionary:");
        logger.trace(controller.print());
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
