package com.korbiak.controller;

import com.korbiak.model.BusinessLogic;

public class ControllerImpl implements Controller {
    BusinessLogic businessLogic;

    public ControllerImpl() {
        this.businessLogic = new BusinessLogic();
    }

    @Override
    public int size() {
        return businessLogic.size();
    }

    @Override
    public boolean isEmpty() {
        return businessLogic.isEmpty();
    }

    @Override
    public String get(Object key) {
        return businessLogic.get(key);
    }

    @Override
    public String put(String key, String value) {
        return businessLogic.put(key, value);
    }

    @Override
    public String remove(Object key) {
        return businessLogic.remove(key);
    }

    @Override
    public String print() {
        return businessLogic.print();
    }
}
