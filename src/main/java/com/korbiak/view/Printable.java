package com.korbiak.view;

@FunctionalInterface
public interface Printable {
  void print();
}
